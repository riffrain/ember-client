/* eslint-env node */
'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {
    babel: {
      includePolyfill: true
    },
    gzip: {
      enabled: false
    },
    vendorFiles: {
      'jquery.js': {
        development: 'bower_components/jquery/dist/jquery.min.js',
        production: 'bower_components/jquery/dist/jquery.min.js'
      }
    },
    minifyCSS: {
      enabled: true,
    },
    minifyJS: {
      enabled: false,
    },
    babel: {
      blocklist: [
        'es6.templateLiterals'
        ,'es6.unicodeRegex'
        ,'flow'
        ,'react'
        ,'reactCompat'
        ,'regenerator'
        ,'strict'
        ,'es7.comprehensions'
        ,'es7.classProperties'
        ,'es7.decorators'
        ,'es7.exportExtensions'
        ,'es7.exponentiationOperator'
        ,'es7.objectSpread'

      ]
    },
    'ember-cli-uglify': {
      enabled: true,
      exclude: ['vendor.js'],
      uglify: {
        compress: false,
        mangle: true
      }
    }
  });

  app.import('bower_components/socket.io-client/dist/socket.io.js');
  app.import('bower_components/google-caja/html-css-sanitizer-minified.js');
  app.import('bower_components/babel-polyfill/browser-polyfill.js', { prepend: true })

  return app.toTree();
};
