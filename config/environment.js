/* eslint-env node */
'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'serial',
    environment,
    rootURL: '/',
    locationType: 'auto',
    apiHost: 'https://serial.o10e.org',
    apiNamespace: 'api',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },

    'ember-websockets': {
      socketIO: true
    },

    'ember-simple-auth': {
    },
    'SemanticUI': {
      import: {
        css: true,
        javascript: false,
        images: true,
        fonts: true
      }
    },
    fastboot: {
      hostWhitelist: ['serial.o10e.org', /^localhost:?\d*$/]
    },
    'ember-cli-uglify': {
      enabled: true,
      exclude: ['vendor.js'],
      uglify: {
        compress: false,
        mangle: true
      }
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {

  }

  return ENV;
};
