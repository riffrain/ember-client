import { inject as service } from '@ember/service';
import Component from '@ember/component';
import { computed } from '@ember/object';
import caja from '../utils/sanitize';

export default Component.extend({
  websocket: service('chat-socket'),
  session: service('session'),
  messageRows: 1,

  didReceiveAttrs() {
    this._super(...arguments);

    this.channel = this.channel || 'general';
    this.userId = this.get("session.data.authenticated.user.id");

    // check connected status
    let channel = this.get('session.channel');

    this.get('websocket').disconnect(channel, this.userId);
    this.set('session.channel', this.channel);

    // connect websocket
    this.get('websocket').connection(this.channel, this.userId);
  },

  didIntertElement() {
    this._super(...arguments);
  },

  didRender() {
    if (this.get('websocket.messagesCount') <= 10) {
      let chat = document.getElementById("chat");
      chat.scrollTop = chat.scrollHeight;
    }
  },

  willDestroyElement() {
    this._super(...arguments);
    console.log('will destroy');
    this.get('websocket').disconnect(this.channel, this.userId);
  },

  didDestroyElement() {
    this._super(...arguments);
    console.log('will destroy');
  },

  messages: computed('websocket.messages', function () {
    return this.get('websocket.messages');
  }),

  actions: {
    /**
     * load more message
     */
    loadMore() {
      return this.get('websocket').loadMore();
    },

    /**
     * when pressed (Ctrl|Meta) + Enter, send message
     */
    inputMessage(event) {
      let keyCode = event.keyCode;
      let pressedCtrl = event.ctrlKey || event.metaKey;

      if (keyCode === 13 && pressedCtrl) {
        this.send('sendMessage');
      }
    },

    /**
     * grow textarea rows
     */
    growRows() {
      let el = document.getElementById('message');
      let lines = el.value.split('\n');
      let lineCount = lines.length;
      if (lineCount >= 1 && lineCount <= 8) {
        this.set('messageRows', lineCount);
      }
    },

    /**
     * send new message
     */
    sendMessage() {
      let el = document.getElementById('message');
      let body = el.value || '';
      if (caja.sanitize(body).replace(/\r?\n/g, '') == '') {
        return;
      }

      let data = {
        "user_id": this.get("session.data.authenticated.user.id"),
        "body": body
      };

      this.get("websocket").sendMessage(data);
      this.set('messageRows', 1);

      el.value = '';
    }
  }
});
