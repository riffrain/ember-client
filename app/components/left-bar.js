import { inject as service } from '@ember/service';
import Component from '@ember/component';

export default Component.extend({
  columns: service('toggle-columns'),
  actions: {
    toggleIssues() {
      this.toggleProperty('columns.isShowIssues');
    }
  }
});
