import Component from '@ember/component';
import { computed } from '@ember/object';
import marky from "npm:marky-markdown";

export default Component.extend({
  item: computed('message', function () {
    let message = this.get('message');
    let formated_body = marky(message.body, {
      highlightSyntax: false,
      prefixHeadingIds: false,
      enableHeadingLinkIcons: false
    });

    return {
      'account': message.user.account,
      'name': message.user.name,
      'body': formated_body.replace(/\r?\n/g, '<br>'),
      'created_at': message.created_at
    };
  }),
});
