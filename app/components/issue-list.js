import { inject as service } from '@ember/service';
import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  store: service("store"),
  appStatus: service("app-status"),

  didIntertElement () {
    this.toggleProperty('appStatus.reload.issue');
  },

  issues: computed('appStatus.reload.issue', function () {
    return this.get("store").findAll("issue");
  }),
});
