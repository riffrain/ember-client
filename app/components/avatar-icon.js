import Component from '@ember/component';
import { computed } from "@ember/object";
import gravatar from 'npm:gravatar';

export default Component.extend({
  src: computed('user', function () {
    return this.get('user.avatar') || gravatar.url(this.get('user.email'), {s: '100', r: 'x', d: 'retro'}, true);
  })
});
