import { inject as service } from '@ember/service';
import Component from '@ember/component';

export default Component.extend({
  tagName: 'nav',
  session: service('session'),

  actions: {
    invalidateSession() {
      this.get('session').invalidate();
    }
  }
});
