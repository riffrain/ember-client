import InfiniteScroller from 'ember-simple-infinite-scroller/components/infinite-scroller';

export default InfiniteScroller.extend({
  _reachedTop() {
    return this._scrollPercentage() <= this._triggerAt();
  },
  _shouldLoadMore() {
    if (!this.get('isReverse')) {
      return this._reachedBottom() && !this.get('isLoading');
    } else {
      return this._reachedTop() && !this.get('isLoading');
    }
  },
});
