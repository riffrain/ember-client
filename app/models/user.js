import DS from 'ember-data';

export default DS.Model.extend({
  name      : DS.attr(),
  account   : DS.attr(),
  email     : DS.attr(),
  password  : DS.attr(),
  bio       : DS.attr(),
  avatar    : DS.attr(),
  created_at    : DS.attr(),
  updated_at    : DS.attr()
});
