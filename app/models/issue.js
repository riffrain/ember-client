import DS from 'ember-data';

export default DS.Model.extend({
  subject     : DS.attr(),
  description : DS.attr(),
  author_id   : DS.attr(),
  user        : DS.attr(),
  created_at  : DS.attr(),
  updated_at  : DS.attr()
});
