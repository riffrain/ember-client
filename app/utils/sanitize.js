/* global html_sanitize */
export default {

  url: function (url) {
    url = url.toString().replace(/['"]+/g, '');
    if (/^https?:\/\//.test(url) || /^\//.test(url)) {
      return url;
    }
  },

  id: function (id) {
      return id;
  },

  sanitize: function (text) {
    return html_sanitize(text, this.url, this.id);
  }
};
