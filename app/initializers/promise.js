import { Promise as EmberPromise } from 'rsvp';

export function initialize() {
  window.Promise = EmberPromise
}

export default {
  name: 'promise',
  initialize
}

