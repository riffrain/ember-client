import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('not-found', {path: '/*path'});

  this.route('dashboard');

  // login
  this.route('login');

  // logout
  this.route('logout');

  this.route('issues', function (){
    // issue list
    this.route('index', {path: '/'});
    // create new issue
    this.route('new');
    // show issue detail
    this.route('detail', {path: '/:id'});
  });

  // sign up
  this.route('users/new', {path: '/signup'});

  // user profile
  this.route('profile', function () {
    this.route('index', {path: '/:account'});
  });

  // current user profile
  this.route('settings', function () {
    this.route('profile');
  });

});

export default Router;
