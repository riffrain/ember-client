import { inject as service } from '@ember/service';
import Controller from '@ember/controller';

export default Controller.extend({
  session: service('session'),
  columns: service('toggle-columns'),

  actions: {
    logout() {
      this.get('session').invalidate();
    }
  }
});
