import { inject as service } from '@ember/service';
import Controller from '@ember/controller';

export default Controller.extend({
  session: service('session'),
  appStatus: service('app-status'),

  actions: {
    async create() {
      let data = this.getProperties('subject', 'description');
      data.author_id = this.get("session.data.authenticated.user.id");

      if (!data.author_id) {
        alert('please login');
        return false;
      }

      if (data.subject === undefined || data.description === undefined) {
        return false;
      }

      this.set('errorMessage', null);
      this.set('successMessage', null);

      const method = "POST";
      const body = JSON.stringify(data);
      const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      };
      let response = await fetch(`/api/issues/add`, {
        method: method, headers: headers, body: body
      });
      let json = await response.json();

      this.set('errorMessage', json.errorMessage);
      if (json.success) {
        this.set('subject', '');
        this.set('description', '');
        this.transitionToRoute('issues.detail', json.issues.id);
      }
    }
  }
});
