import Controller from '@ember/controller';
import fetch from 'fetch';

export default Controller.extend({
  actions: {
    create: function () {
      var self = this;
      var data = this.getProperties('account', 'name', 'email', 'password', 'confirm');

      self.set('errorMessage', null);
      self.setProperties({'password':null , 'confirm': null});

      fetch('/api/users/add', {
        method: 'POST',
        body:    data,
        headers: { 'Content-Type': 'application/json' },
      })
      .then(function (res) {
        self.set('errorMessage', res.errorMessage);
        if (res.success) {
          self.transitionToRoute('dashboard');
        }
      });

    }
  }
});
