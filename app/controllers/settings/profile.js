import $ from 'jquery';
import { later } from '@ember/runloop';
import { inject as service } from '@ember/service';
import Controller from '@ember/controller';
import gravatar from 'npm:gravatar';
import fetch from 'fetch';

export default Controller.extend({
  session:     service('session'),
  currentUser: service('current-user'),

  init() {
    this._super(...arguments);

    this.set('user', this.get('currentUser.user'));
    let src = this.get('user.avatar') || gravatar.url(this.get('user.email'), {s: '100', r: 'x', d: 'retro'}, true);
    this.set('src', src);
  },

  async uploadImage(userId) {
    if (!this.get('new_avatar')) {
      return false;
    }

    // upload image
    let fd = new FormData();
    fd.append('avatar', this.get('new_avatar'));
    let response = await fetch("/api/users/avatar/" + userId, {
      method : "POST",
      body : fd
    });
    let json = await response.json();

    return json.filename;
  },

  async loadCurrentUserData(userId) {
    let model = await this.get('store').findRecord('user', userId);
    return model;
  },

  clearMessage() {
    this.set('flashMessage', null);
    this.set('flashMessageClass', null);
  },

  src: null,
  new_avatar: null,
  avatarFileName: 'No file chosen',
  actions: {
    async update() {
      // clear flash message
      this.clearMessage();

      // upload image and load user data
      let userId = this.get('session.data.authenticated.user.id');
      let filename = await this.uploadImage(userId);
      let model = await this.loadCurrentUserData(userId);

      // save user data
      let user = this.get('user');
      let {name, email, bio} = user.getProperties('name', 'email', 'bio');
      model.set('name', name);
      model.set('email', email);
      model.set('bio', bio);
      if (filename) {
        model.set('avatar', filename);
      }
      await model.save();

      // show message
      this.set('flashmessage', 'saved!');
      later(() => {
        this.set('flashmessage', null);
        this.set('flashmessageclass', 'animated fade hidden transition');
      }, 1500);
    },

    preview(event) {
      let filename = event.target.value || 'No file chosen';
      this.set('avatarFileName', filename.replace(/^.*[\\/]/, ''));

      let blob = event.target.files[0];
      if (!blob) {
        return;
      }
      this.set('new_avatar', blob);

      let fileReader = new FileReader();
      fileReader.onload = (event) => {
        this.set('src', event.target.result);
      }
      fileReader.readAsDataURL(blob);
    },

    browseFile() {
      $('#avatar').click();
    }
  }
});
