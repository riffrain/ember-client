import { inject as service } from '@ember/service';
import Controller from '@ember/controller';

export default Controller.extend({
  session: service('session'),

  actions: {
    authenticate : function () {
      var self = this;
      let {email, password} = this.getProperties('email', 'password');

      this.get('session').authenticate('authenticator:oauth2', email, password)
        .catch(function (reason) {
          self.set('errorMessage', reason.error);
        });
    }
  }
});
