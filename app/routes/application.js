import { inject as service } from '@ember/service';
import Route from '@ember/routing/route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Route.extend(ApplicationRouteMixin, {
  session: service('session'),
  //fastboot: service('fastboot'),
  currentUser: service('current-user'),
  beforeModel() {
    return this._loadCurrentUser();
  },
  _loadCurrentUser() {
    return this.get('currentUser')
      .load()
      .catch(() =>
        this.get('session').invalidate()
      );
  },
  sessionAuthenticated() {
    this._loadCurrentUser();
  },
});
