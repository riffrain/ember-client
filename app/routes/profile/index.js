import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    let account = params.account;
    return this.get("store").findRecord("user", account);
  },
  setupController: function (controller, model) {
    controller.set('model', model);
  }
});
