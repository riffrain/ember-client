import Service, { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { resolve } from 'rsvp';

export default Service.extend({
  session: service('session'),
  store: service(),

  load() {
    let userAccount = this.get('session.data.authenticated.user.account');
    if (!isEmpty(userAccount)) {
      return this.get('store')
        .findRecord('user', userAccount)
        .then((user) => {
          this.set('user', user);
        });
    } else {
      return resolve();
    }
  }
});
