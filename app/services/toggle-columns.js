import Service from '@ember/service';

export default Service.extend({
  isShowIssues: false,
  toggleIssue: function () {
    this.toggleProperty('isShowIssues');
  }
});
