/* global io */
import Service, { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Service.extend({
  store: service('store'),
  messages: [],
  pagination: {
    page: 1,
    pageSize: 20,
  },

  messagesCount: computed('messages.[]', function () {
    return this.get('messages').length;
  }),

  connection(channel, userId) {
    var socket = this.socket = io();

    this.getPagination().then((res) => {
      this.set('pagination', res.paginate);
      this.storeMessages();
    });

    // join room
    socket.emit('join', { roomid: channel, id: userId});

    /**
     * call from ../../../socket.js
     */
    socket.on("receiveMessage", (data) => {
      let message = data.message;
      this.messages.pushObject({
        "user": message.user,
        "body": message.body,
        "created_at": message.created_at
      });
    });
  },

  disconnect(channel, userId) {
    if (!this.socket) {
      return;
    }

    console.log('chat-socke');
    this.socket.disconnect();
  },

  /**
   * get pagination of message
   */
  async getPagination () {
    let response = await fetch('/api/messages/page');
    let json = await response.json();

    return json;
  },

  /**
   * restore messages
   */
  storeMessages() {
    this.get('store').query('message', this.pagination).then((data) => {
      if (data.content.length === 0) {
        return;
      }
      var messages = [];
      data.forEach(function (message) {
        messages.unshift({
          "user": message.get('user'),
          "body": message.get('body'),
          "created_at": message.get('created_at')
        });
      });
      this.messages.unshiftObjects(messages);
    });
  },

  /**
   * get more messages
   */
  loadMore() {
    if (this.pagination.page + 1 > this.pagination.pageCount) {
      return false;
    }
    this.pagination.page++;

    return this.get('store').query('message', this.pagination).then((data) => {
      data.forEach((message) => {
        this.messages.unshiftObject({
          "user": message.get('user'),
          "body": message.get('body'),
          "created_at": message.get('created_at')
        });
      });
      return true;
    });
  },

  /**
   * sendMessage
   * call server side event
   * @param {"user_id": integer, "message": string}
   */
  sendMessage(data) {
    this.socket.emit("deliverMessage", data);
  }
});
